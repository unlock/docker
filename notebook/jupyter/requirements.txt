altair==4.1.0
async-generator==1.10
blinker==1.4
brotlipy==0.7.0
certifi==2021.5.30
certipy==0.1.3
conda==4.10.3
cycler==0.10.0
cytoolz==0.11.0
Distance==0.1.3
gmpy2==2.1.0b5
HeapDict==1.0.1
importlib==1.0.4
ipython-genutils==0.2.0
isodate==0.6.0
jupyter-contrib-core==0.3.3
jupyter-contrib-nbextensions==0.5.1
jupyter-highlight-selected-word==0.2.0
jupyter-latex-envs==1.4.6
jupyter-nbextensions-configurator==0.4.1
llvmlite==0.36.0
locket==0.2.0
lxml==4.6.3
networkx==2.3
pamela==1.0.0
pandas==1.3.2
pandocfilters==1.4.2
patsy==0.5.1
prettytable==2.1.0
protobuf==3.17.2
pybind11==2.7.1
pycurl==7.44.1
pyparsing==2.4.7
python-editor==1.0.4
python-irodsclient==1.0.0
PyYAML==5.4.1
rdflib==6.0.0
requests-unixsocket==0.2.0
scikit-image==0.18.3
simplegeneric==0.8.1
SPARQLWrapper==1.8.5
webencodings==0.5.1
xmlrunner==1.7.7
zict==2.0.0