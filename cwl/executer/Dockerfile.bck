# Base Image
FROM ubuntu:18.04

# Metadata
LABEL base.image="docker-registry.wur.nl/unlock/docker"
LABEL version="1"
LABEL software="BASE 1.0"
LABEL software.version="0.0.0"
LABEL description="IRODS base image for UNLOCK"
LABEL website="https://m-unlock.gitlab.io"
LABEL documentation="NA"
LABEL license="NA"
LABEL tags="Base"

ARG R_VERSION=4.1.1
ARG OS_IDENTIFIER=ubuntu-1804
# ARG cwltool_version=3.1.20211004060744

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8

# Some default needed stuff
# RUN apt-get update && apt-get install -y git build-essential curl wget nano htop pigz zip unzip

# Fix issue with scipy no lapack/blas resources found and other compilation problems
# Install ubuntu modules and the workflow application # --no-install-recommends 
RUN apt-get update && apt-get install --yes build-essential cpanminus curl git gnupg htop libfontconfig1 locales nano nodejs pigz python3-dev python3-distutils python3.8 raptor2-utils raptor2-utils sshpass unzip wget zip libblas-dev liblapack-dev zlib1g-dev pkg-config libhdf5-100 libhdf5-dev gfortran libblas-dev liblapack-dev zlib1g-dev pkg-config libhdf5-100 libhdf5-dev gfortran libbz2-dev python-setuptools libidn11 rsync lftp

# Personal cwltool with some additional hacks
RUN	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python3.8 get-pip.py && \
	python3.8 -m pip install git+https://github.com/jjkoehorst/cwltool.git@cwlprov-cwl-rdf &&\
	cwltool --version && python3.8 -m pip install html5lib

# For docker in docker
RUN apt-get update && \
	apt-get install --yes ca-certificates curl gnupg lsb-release && \
	mkdir -p /etc/apt/keyrings && \
	apt-get install --yes curl && \
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
	apt-get update && \
	apt-get install --yes docker-ce docker-ce-cli containerd.io docker-compose-plugin

# for rdfhash python version 3.10 is needed
RUN apt install software-properties-common -y && \
	add-apt-repository ppa:deadsnakes/ppa && \
	apt install --yes python3.10 && \
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python3.10 get-pip.py && \
	python3.10 -m pip install rdfhash

# Enable icommands
RUN python3.8 -m pip install python-irodsclient --upgrade && \
	apt-get update && \
	wget -qO - https://packages.irods.org/irods-signing-key.asc | apt-key add - && \
	echo "# /etc/apt/sources.list.d/renci-irods.list" | tee -a /etc/apt/sources.list.d/renci-irods.list && \
	echo "deb [arch=amd64] https://packages.irods.org/apt/ bionic main" | tee -a /etc/apt/sources.list.d/renci-irods.list && \
	apt-get update && apt-get install --no-install-recommends --yes irods-icommands

# Couple shell to bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Java installation
RUN curl -s https://get.sdkman.io | bash
RUN chmod a+x "$HOME/.sdkman/bin/sdkman-init.sh"
RUN echo "sdkman_auto_complete=false" >> "$HOME/.sdkman/etc/config"
RUN source "$HOME/.sdkman/bin/sdkman-init.sh" &&\
	sdk install java 17.0.1-tem &&\
 	sdk install gradle 7.3.1 && \
 	sdk install maven 3.8.4 && \
	rm  -rf /root/.sdkman/archives

# install R
RUN wget https://cdn.rstudio.com/r/${OS_IDENTIFIER}/pkgs/r-${R_VERSION}_1_amd64.deb && \
    apt-get update -qq && \
    DEBIAN_FRONTEND=noninteractive apt-get install -f -y ./r-${R_VERSION}_1_amd64.deb && \
    ln -s /opt/R/${R_VERSION}/bin/R /usr/bin/R && \
    ln -s /opt/R/${R_VERSION}/bin/Rscript /usr/bin/Rscript && \
    ln -s /opt/R/${R_VERSION}/lib/R /usr/lib/R && \
    rm r-${R_VERSION}_1_amd64.deb && \
    rm -rf /var/lib/apt/lists/*

# Miniconda
RUN wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
	bash /Miniconda3-latest-Linux-x86_64.sh -b -p $HOME/miniconda && \
	rm Miniconda3-latest-Linux-x86_64.sh && \
	source /root/miniconda/bin/activate && \
	conda update -y -n base -c defaults conda && \
	conda install -y mamba -n base -c conda-forge
	# And clean?


######################################
####### ANALYSIS TOOLS SECTION #######

# Fix locale issue with quast
RUN apt-get clean && apt-get update && locale-gen en_US.UTF-8 && dpkg-reconfigure locales

## Needed for faTools (used in the metagenomics workflow)
RUN sudo ln -s /usr/lib/x86_64-linux-gnu/libpng16.so.16.34.0 /usr/lib/x86_64-linux-gnu/libpng12.so.0

## SET PATH ##
# Miniconda
ENV CONDA=/root/miniconda/bin
# JAVA SDKMan
ENV SDKMAN=/root/.sdkman/candidates/maven/current/bin:/root/.sdkman/candidates/java/current/bin:/root/.sdkman/candidates/gradle/current/bin
# CheckM
ENV CHECKM=/unlock/infrastructure/binaries/hmmer/hmmer-3.3.2/bin:/unlock/infrastructure/binaries/pplacer/pplacer-Linux-v1.1.alpha17/bin:/unlock/infrastructure/binaries/prodigal/v2.6.3
# note: CheckM and GTDB-Tk overlap with hmmer,prodigal and are not added to GTDBTK
ENV GTDBTK=/unlock/infrastructure/binaries/Mash/mash-Linux64-v2.3:/unlock/infrastructure/binaries/FastTree/FastTree_v2.1.11:/unlock/infrastructure/binaries/Mash/mash-Linux64-v2.3:/unlock/infrastructure/binaries/fastANI/fastANI_v1.33
## GTDB-Tk
ENV GTDBTK_DATA_PATH=/unlock/references/databases/GTDBTK/release202/
# All paths combined
ENV PATH=$PATH:$SDKMAN:$CHECKM:$GTDBTK:$CONDA

######################################
######################################

# Add anonymous access
COPY irods /root/.irods

# Git config file for the runner?
COPY gitconfig /root/.gitconfig

# Add the startup script
COPY run.sh run.sh
COPY execute.sh /usr/bin/execute.sh

# Make it executable
RUN chmod +x run.sh
RUN chmod +x /usr/bin/execute.sh

# Add the upload script and install sshpass
COPY /scripts/upload.sh upload.sh

# Scripts for small management tasks
COPY scripts /scripts

# Conda environments
COPY conda /conda

# Set different location for R packages
# COPY scripts/Renviron /root/.Renviron

# Extend bashrc
RUN echo "export R_LIBS=/unlock/infrastructure/R/library" >> ~/.bashrc
RUN echo "export R_LIBS_SITE=/unlock/infrastructure/R/library" >> ~/.bashrc
RUN echo "export R_LIBS_USER=/unlock/infrastructure/R/library" >> ~/.bashrc