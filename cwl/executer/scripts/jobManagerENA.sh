# Script to download and star the iRODSKubernetes program
wget -N http://download.systemsbiology.nl/unlock/iRODSKubernetes.jar

# # Run once per day
while true
do
    # Runs every minute if the connection fails
    while true
    do
        java -jar iRODSKubernetes.jar -kubernetes -project "references" -limit 25

        EC=$?

        if [ $EC -eq 0 ]; then
            break
        fi

        sleep 1m
    done
    sleep 1h
done