from irods.session import iRODSSession
import ssl
from irods.meta import iRODSMeta
import os, sys
import gzip
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-file", help="Path to the file on the irods system of which the metadata needs to be modified", required=True)

args = parser.parse_args()

# iRODS authentication information
host = os.getenv('irodsHost')
port = os.getenv('irodsPort')
zone = os.getenv('irodsZone')
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {'irods_client_server_negotiation': 'request_server_negotiation',
                'irods_client_server_policy': 'CS_NEG_REQUIRE',
                'irods_encryption_algorithm': 'AES-256-CBC',
                'irods_encryption_key_size': 32,
                'irods_encryption_num_hash_rounds': 16,
                'irods_encryption_salt_size': 8,
                'ssl_context': context}

# Authentication
with iRODSSession(
    host = host,
    port = port,
    user = user,
    password = password,
    zone = zone,
    **ssl_settings) as session:

    with gzip.open(args.file,'r') as fin: 
        count = 0
        reads = 0
        bases = 0
        length = 0   
        for index, line in enumerate(fin):
            count = count + 1
            if count == 2:
                line = line.decode("ascii")
                reads = reads + 1
                bases = bases + len(line.strip())
                if len(line.strip()) > length:
                    length = len(line.strip())
            if count == 4:
                count = 0
    directory = os.path.abspath(os.path.dirname(__file__))
    
    command = "python3 " + directory + "/metadata.py -add -file " + args.file + " -key reads -value " + str(reads)
    os.system(command)
    command = "python3 " + directory + "/metadata.py -add -file " + args.file + " -key bases -value " + str(bases)
    os.system(command)
    command = "python3 " + directory + "/metadata.py -add -file " + args.file + " -key length -value " + str(length)
    os.system(command)