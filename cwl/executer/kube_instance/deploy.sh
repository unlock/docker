#!/bin/bash
#============================================================================
#title          :Instance creation for unlock nodes
#description    :Creates an sleeping instance on each of the unlock nodes
#author         :Jasper Koehorst & Bart Nijsse
#date           :2021
#version        :0.0.2
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Clean up finished sync pods
kubectl get pods --namespace='unlock' | grep munlock-instance | grep Completed | awk '{print "kubectl -n unlock delete pod "$1}' | sh
# Get all possible nodes
kubectl get nodes -l node-role.kubernetes.io/worker=true | grep -v "SchedulingDisabled" | awk '{print $1}' | grep -v NAME > $DIR/nodes.txt

while read node; do
    sed "s/HOSTNAME/$node/g" $DIR/template_instance.yaml > $DIR/$node\_instance.yaml
    kubectl apply -f $DIR/$node\_instance.yaml
    rm $DIR/$node\_instance.yaml
done < $DIR/nodes.txt