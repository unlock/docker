#!/bin/bash
#============================================================================
#title          :Docker base image
#description    :Builds the base image for unlock
#author         :Jasper Koehorst
#date           :2022
#version        :0.0.1
#============================================================================

docker login git@docker-registry.wur.nl  

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================

docker pull docker-registry.wur.nl/unlock/docker/debian:buster

docker build -t docker-registry.wur.nl/unlock/docker/debian:buster .

docker push docker-registry.wur.nl/unlock/docker/debian:buster